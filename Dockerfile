# Create and build in 2 images
FROM maven:3.8.1-openjdk-17 AS builder
WORKDIR /app
COPY pom.xml .
RUN mvn -e -B dependency:resolve
COPY src ./src
RUN mvn clean -e -B package

FROM openjdk:slim
WORKDIR /app
COPY --from=builder /app/target/wordle-1.0-SNAPSHOT.jar .
ENTRYPOINT ["java", "-jar", "./wordle-1.0-SNAPSHOT.jar"]
